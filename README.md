gulfscei.vagrant
=========

A role to configure the base Vagrant VM to be ready for Rails development

Requirements
------------

None

Role Variables
--------------

```
# The name of the console prompt
console_name: Vagrant-VM

# this capistrano path for the deployment directory
app_path: /home/webapps/apps/rails_server

# this is the directory path where the app stores the files, omit trailing slash
#file_storage_path: storage

# the path to the RVM installation
rvm_path: /usr/local/rvm/bin/rvm

# If the application is mounted on a sub uri
#application_sub_uri: app

# any additional packages outside fo the standard ones being installed
vagrant_additional_packages: []

# add the heroku task so we can install heroku on the vagrant box
vagrant_enable_heroku: no

# enable the nginx server to be access from the http protocol
nginx_allow_http_access: no
```

Dependencies
------------

None

Requirements File
-----------------

```
- src: git@gitlab.com:TridentUno/ansible-role-vagrant.git
  scm: git
  version: v0.2
  name: gulfscei.vagrant
```


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: gulfscei.vagrant }

License
-------

MIT

Author Information
------------------

Contact `drward3@uno.edu` for more information about this role

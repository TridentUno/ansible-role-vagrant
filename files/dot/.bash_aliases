alias rails_start='cd ~/workspace && bundle exec pumactl --config-file /vagrant/config/puma.rb start -d && cd -'
alias rails_stop='cd ~/workspace && bundle exec pumactl --config-file /vagrant/config/puma.rb stop -d && cd -'
alias rails_restart='cd ~/workspace && bundle exec pumactl --config-file /vagrant/config/puma.rb restart -d && cd -'
alias rails_log='tail -f -n 400 /vagrant/log/development.log'
alias puma_log='tail -f -n 400 /vagrant/log/puma.stdout.log'
alias byebug_connect='cd /vagrant && byebug -R localhost:1048 && cd -'
alias be='bundle exec'
